# Azure

Login and configure the correct subscription:

```bash
az login
az account set --subscription="4f3dc88c-17df-4a84-b64d-cb589647a187"
```

Then initialize Terraform with:

```bash
terraform init
```

Plan changes with:

```bash
terraform plan
```

And apply the changes with:

```bash
terraform apply
```
