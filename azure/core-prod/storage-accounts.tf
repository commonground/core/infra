resource "azurerm_storage_account" "coreprodterraformstate" {
  name                     = "coreprodterraformstate"
  resource_group_name      = azurerm_resource_group.core-prod.name
  location                 = azurerm_resource_group.core-prod.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "production"
  }
}

resource "azurerm_storage_account" "coreprodhelmrepository" {
  name                     = "coreprodhelmrepository"
  resource_group_name      = azurerm_resource_group.core-prod.name
  location                 = azurerm_resource_group.core-prod.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    environment = "production"
  }
}
