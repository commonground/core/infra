resource "azurerm_storage_container" "terraform-state" {
  name                  = "terraform-state"
  storage_account_name  = azurerm_storage_account.coreprodterraformstate.name
  container_access_type = "private"
}

resource "azurerm_storage_container" "helm-repository" {
  name                  = "helm-repository"
  storage_account_name  = azurerm_storage_account.coreprodhelmrepository.name
  container_access_type = "private"
}