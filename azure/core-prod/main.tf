provider "azurerm" {
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name  = "core-prod"
    storage_account_name = "coreprodterraformstate"
    container_name       = "terraform-state"
    key                  = "core-prod.tfstate"
  }
}
