# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/azurerm" {
  version = "2.45.0"
  hashes = [
    "h1:I1xlvEcj4TRvc/omViPDyEOh4aTjZ/wwlx5AK5xCp9s=",
    "zh:0a583023e822dee0abc82f175956574dcd9e33e32634b91c205b3511873c8f38",
    "zh:0e21073d5ad7c54b145722f58159e2c92f18877ce4db154498968b027189f482",
    "zh:3bb7d4aecf266a47f05232aba136939e45ab73e14741b0d3e7160b8319fb6c41",
    "zh:5d9547145d90ed1f7e1caa0db80a1adfcafb48a7a4deae54d88bc1bec90b0380",
    "zh:6c9b3af08f158d81c48f797cc258df9783d0cafe83fedfffc500c3ec1b4e7cab",
    "zh:8231960ca8509e5493d7d2affc2da3a6c044220461f950ce7296164d29a7b5f6",
    "zh:956572b64677fe9732bf8bebaa3ff51134acec948698e93705b348142bee42c8",
    "zh:99af8e398f5aebbb1714474bc267a36b8c09feea9fc0bd6b64f8042aee89f092",
    "zh:e6bf30cbfcd46d4888fd60db35a4bbe4b80f1aea77d87ed25b8569c63947c383",
    "zh:f201d722a9e7939a38172e6ebcfdd61137e99ba13365ae9d8c6922d88d489a20",
  ]
}
